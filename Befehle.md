### create docker network
docker network create todoapp_network

### show docker network
docker network ls

### build a docker image
docker build -t todo-app:v1 .

### start the app
docker run --net=todoapp_network --name=frontend -d -p 3000:3000 todo-app:v1

### show the log
docker logs <container id>

### stream  the log
docker logs -f <container id> 

### remove a container
docker rm todo-app:v1

### remove a image
docker rmi todo-app:v1

### System bereinigen
docker system prune -a --volumes  #Löscht alle Conatiner, Images, Volumes vom System. 
